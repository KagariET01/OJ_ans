# OJ\_ans
　　此github資料庫為[ET01](../../../)一個人完成。這邊提供熱門Online Judge的題解，但是看完題解後請自行思考其原理，且要自己重新寫一遍，不要沒思考過後就直接送出此答案<br>
如果對題解有任何疑問，請嘗試[提出Issue](../../../oj_ans/issues/new/choose)<br>
[目次]
> [Algorithm](#algorithm)  
> [AtCoder](/AtCoder/README.md)  
> [Codeforces](/codeforces/README.md)  
> [leetcode](#leetcode)  
> [neoj](/neoj/README.md)  
> [racing](#racing)  
> [tcirc](#tcirc)  
> [TIOJ](#tioj)  
> [Zerojudge](#zerojudge)  
> [APCS](#apcs)  
> [PCIC](#PCIC)  
>
# 檔案
> ## [Algorithm](/Algorithm)
>> [DP](/Algorithm/dp/)
>>> 未整理 [LCS](/Algorithm/dp/LCS/)
>>>> [Q](/Algorithm/dp/LCS/LCS_1.cpp)  
>>>> [temp](/Algorithm/dp/LCS/LCS_2_template.cpp)
>>>>
>>> 未整理 [LIS](/Algorithm/dp/LIS/)
>>>> [Q](/Algorithm/dp/LIS/LIS_1.cpp)  
>>>> [temp](/Algorithm/dp/LIS/LIS_2_template.cpp)  
>>>> [temp 2](/Algorithm/dp/LIS/LIS_3_template.cpp)
>>>>
>>> 未整理 [01背包問題](/Algorithm/dp/TFbackpack/)
>>>> [經典版](/Algorithm/dp/TFbackpack/TFbackpack_q_1.cpp)  
>>>> [變形版](/Algorithm/dp/TFbackpack/TFbackpack_mult_item.cpp)
>>>>
>> [Math](/Algorithm/math/)
>>> [Geometry](/Algorithm/Geometry/Geometry.cpp)  
>>> [Mod](/Algorithm/math/mod/)
>>>> [Extended\_Euclidean 擴展歐幾里得](/Algorithm/math/Extended_Euclidean.cpp)  
>>>> [Modular\_multiplicative\_inverse 模逆元](/Algorithm/math/mod/Modular_multiplicative_inverse.cpp)
>>>>
>>> [四則運算](/Algorithm/math/四則運算.cpp)  
>>> [矩陣快速冪](/Algorithm/math/矩陣快速冪-q1.cpp)
>>>
>> [String](/Algorithm/string)
>>> KMP
>>>> [md](/Algorithm/string/kmp.md)  
>>>> [C++](/Algorithm/string/kmp.cpp)
>>>>
>>> Z-value
>>>> [md](/Algorithm/string/z-value.md)  
>>>> [C++](/Algorithm/string/z-value.cpp)
>>>>
>>> [hash (未測試)](/Algorithm/string/hash.cpp)  
>>> [trie (未測試)](/Algorithm/string/trie.cpp)  
>>> [Failure Function](/Algorithm/string/Failure.cpp)
>>>
>> [tree](/Algorithm/tree/)
>>> [最短路徑](/Algorithm/tree/最短路徑/)
>>>> 未整理 [1](/Algorithm/tree/最短路徑/最短路徑-1.cpp)  
>>>> 未整理 [greedy 虛擬碼](/Algorithm/tree/最短路徑/最短路徑-greedy-虛擬碼.txt)  
>>>> 未整理 [greedy](/Algorithm/tree/最短路徑/最短路徑-greedy.cpp)  
>>>> 未整理 [DFS](/Algorithm/tree/最短路徑/最短路徑dfs.cpp)  
>>>> [Dijkstra 最短路徑](/Algorithm/tree/dijkstra.cpp)
>>>>
>>> [BFS](/Algorithm/tree/BFS/)
>>>> WA [Topological sorting](/Algorithm/tree/BFS/Topological_sorting/Topological_sorting_q_1.cpp)  
>>>> [q1](/Algorithm/tree/BFS/bfs_q_1.cpp)  
>>>> [q2](/Algorithm/tree/BFS/bfs_q_2.cpp)
>>>>
>>> [lca](/Algorithm/tree/lca/lca_1.cpp)  
>>> [rmq](/Algorithm/tree/rmq/rmq_1.cpp)  
>>> [BIT 樹狀數組](/Algorithm/tree/BIT.cpp)  
>>> [dsu](/Algorithm/tree/dsu.cpp)  
>>> [Segment Tree 線段數](/Algorithm/tree/seg_tree.cpp)  
>>> 未整理 [樹直徑 Q](Algorithm/tree/樹直徑_q_1.cpp)  
>>>
>> [basic 基本樣板](Algorithm/basic_lite.cpp)  
>> [DC 分治](Algorithm/DC_1.cpp) 
>>
> ## [leetcode](/leetcode)
>> [lcode 1 Two Sum](/leetcode/1.cpp)  
>> [lcode 239 sliding-window-maximum](/leetcode/239.cpp)  
>> [lcode 392 Is Subsequence](/leetcode/392-summit.cpp)  
>> [lcode 704 Binary Search](/leetcode/704.cpp)
> ## [racing](/racing/)
>> [TOI](/racing/TOI/)/[練習賽](/racing/TOI/練習賽/)/[2023](/racing/TOI/練習賽/2023/)/[04](/racing/TOI/練習賽/2023/04/)  
>>> [新手組](/racing/TOI/練習賽/2023/04/新手組)  
>>>> pA [Q](/racing/TOI/練習賽/2023/04/新手組/pA.pdf) , [cpp](/racing/TOI/練習賽/2023/04/新手組/pA.cpp)  
>>>> pB [Q](/racing/TOI/練習賽/2023/04/新手組/pB.pdf) , [cpp](/racing/TOI/練習賽/2023/04/新手組/pB.cpp)  
>>>> pC [Q](/racing/TOI/練習賽/2023/04/新手組/pC.pdf) , [cpp](/racing/TOI/練習賽/2023/04/新手組/pC.cpp)  
>>>> 
>>> [潛力組](/racing/TOI/練習賽/2023/04/潛力組)  
>>>> pA [Q](/racing/TOI/練習賽/2023/04/潛力組/pA.pdf) , [cpp](/racing/TOI/練習賽/2023/04/潛力組/pA.cpp)  
>>>> pB [Q](/racing/TOI/練習賽/2023/04/潛力組/pB.pdf) , [cpp](/racing/TOI/練習賽/2023/04/潛力組/pB.cpp)  
>>>> pC [Q](/racing/TOI/練習賽/2023/04/潛力組/pC.pdf) , [cpp](/racing/TOI/練習賽/2023/04/潛力組/pC.cpp)  
>>> 
>> [資訊之芽](/racing/資訊之芽)/[2023](/racing/資訊之芽/2023)
>>> [期中考](/racing/資訊之芽/2023/期中考)
>>>> pA [Q](/racing/資訊之芽/2023/期中考/pA.pdf) , [cpp](/racing/資訊之芽/2023/期中考/pA.cpp)  
>>>> pB [Q](/racing/資訊之芽/2023/期中考/pB.pdf)  
>>>> pC [Q](/racing/資訊之芽/2023/期中考/pC.pdf) , [cpp](/racing/資訊之芽/2023/期中考/pC.cpp)  
>>>> pD [Q](/racing/資訊之芽/2023/期中考/pD.pdf)  
>>>> pE [Q](/racing/資訊之芽/2023/期中考/pE.pdf)  
>>>>
>>> [期末考](/racing/資訊之芽/2023/期末考)
>>>> pA [Q](/racing/資訊之芽/2023/pA.pdf) , cpp/[20%](/racing/資訊之芽/2023/pA.cpp)  
>>>> pB [Q](/racing/資訊之芽/2023/pB.pdf)  
>>>> pC [Q](/racing/資訊之芽/2023/pC.pdf)  
>>>> pD [Q](/racing/資訊之芽/2023/pD.pdf)  
>>>> pE [Q](/racing/資訊之芽/2023/pE.pdf)  
>>>> pF [Q](/racing/資訊之芽/2023/pF.pdf)  
>>>> pG [Q](/racing/資訊之芽/2023/pG.pdf)  
>>>> pH [Q](/racing/資訊之芽/2023/pH.pdf)  
>>> 
> ## [Tcirc](/tcirc)
>> [tcirc d030](/tcirc/tcirc_d030.cpp)  
>> [tcirc d075](/tcirc/tcirc_d075.cpp)
>>
> ## [TIOJ](/tioj)
>> [TIOJ 1015 Squares in Rectangle](/tioj/tioj_1015.cpp)  
>> [TIOJ 1040 連分數](/tioj/tioj_1040.cpp)  
>> [TIOJ 1072 誰先晚餐](/tioj/tioj_1072.cpp)  
>> [TIOJ 1105](/tioj/tioj_1105.cpp)  
>> [TIOJ 1152 銀河帝國旅行社](/tioj/tioj_1152.cpp)  
>> [TIOJ 1199 神奇的模術](/tioj/tioj_1199.cpp)  
>> [TIOJ 1402 淹水問題](/tioj/tioj_1402.cpp)  
>> [TIOJ 1419](/tioj/tioj_1419.cpp)  
>> [TIOJ 1947 小向的試煉 1-3：森林(Forest)](/tioj/tioj_1947.cpp)  
>> [TIOJ 2252](/tioj/tioj_2252.cpp)  
>> [TIOJ 2253](/tioj/tioj_2253.cpp)  
>>
> ## [Zerojudge](/zerojudge)
>> zj a001 哈囉 [cpp](/zerojudge/zj_a001.cpp) , [py](/zerojudge/zj_a001.py)  
>> [zj a002 簡易加法](/zerojudge/zj_a002.cpp)  
>> [zj a003 兩光法師占卜術](/zerojudge/zj_a003.cpp)  
>> [zj a004 文文的求婚](/zerojudge/zj_a004.cpp)  
>> [zj a005 Eva 的回家作業](/zerojudge/zj_a005.cpp)  
>> [zj a006 一元二次方程式](/zerojudge/zj_a006.cpp)  
>> [zj a009 解碼器](/zerojudge/zj_a009.cpp)  
>> zj a017 五則運算/[py](/zerojudge/zj_a017.py)  
>> [zj a289 Modular Multiplicative Inverse](/zerojudge/zj_a289.cpp)  
>> [zj a597 祖靈被榨乾了!!!!!!!!](/zerojudge/zj_a597.cpp)  
>> [zj b430 簡單乘法](/zerojudge/zj_b430.cpp)  
>> [zj c067 Box of Bricks](/zerojudge/zj_c067.cpp)  
>> [zj c291 小群體](/zerojudge/zj_c291.cpp)  
>> [zj f314 勇者修煉](/zerojudge/zj_f314.cpp)  
>> [zj f581 圓環出口](/zerojudge/zj_f581.cpp)  
>> zj g541 [cpp](/zerojudge/zj_g541.cpp) , [md](/zerojudge/zj_g541.md)  
>> [zj h026 猜拳](/zerojudge/zj_h026.cpp)  
>> [zj h028 砍樹](/zerojudge/zj_h028.cpp)  
>> [zj h399 h400 蛋糕店促銷](/zerojudge/zj_h399_h400.cpp)  
>> [zj i399 數字遊戲](/zerojudge/zj_h028.cpp)  
>> [zj k465 姓名分析 (Name)](/zerojudge/zj_k465.cpp)  
>> 
> ## APCS
>> 2020  
>>> 01  
>>>> [1 (zj_h026)](/zerojudge/zj_h026.cpp)    
>>>> [3 (zj_h028](/zerojudge/zj_h028.cpp)    
>>>> [3 (tcirc_d030)](/tcirc/tcirc_d030.cpp)  
>>>>  
>>> 07  
>>>> [3 (zj_f581)](/zerojudge/zj_f581.cpp)  
>>>>  
>>> 10  
>>>> [3 (zj_f314)](/zerojudge/zj_f314.cpp)  
>>>>  
>> 2018  
>>> 10  
>>>> [4 (zj_d075)](/zerojudge/zj_d075.cpp)  
>>>>  
>> 2017  
>>> 03  
>>>> [2 (zj_c291) 小群體](/zerojudge/zj_c291.cpp)  
>>>> 
> ## PCIC
>> [2023](/PCIC/2023/)/
>>> [test](/PCIC/2023/test/)
>>>> pA [Q](/PCIC/2023/test/pA.pdf) , [cpp](/PCIC/2023/test/a.cpp)  
>>>> pB [Q](/PCIC/2023/test/pB.pdf) , [cpp](/PCIC/2023/test/b.cpp)  
>>>> pC [Q](/PCIC/2023/test/pC.pdf) , [cpp](/PCIC/2023/test/c.cpp)  
>>>> pD [Q](/PCIC/2023/test/pD.pdf)
>>>> 
>>> [r1](/PCIC/2023/r1/) ***`FINISH`***
>>>> pA [Q](/PCIC/2023/test/pA.pdf) , [cpp](/PCIC/2023/r1/pA.cpp)  
>>>> pB [Q](/PCIC/2023/test/pB.pdf) , [cpp](/PCIC/2023/r1/pB.cpp)  
>>>> pC [Q](/PCIC/2023/test/pC.pdf) , cpp/[100](/PCIC/2023/r1/pC.cpp) ~~, cpp/[049](/PCIC/2023/r1/pC_racing.cpp)~~  
>>>> pD [Q](/PCIC/2023/test/pD.pdf) , cpp/[100](/PCIC/2023/r!/pD.cpp) ~~, cpp/[000](/PCIC/2023/r1/pD_racing.cpp)~~  
>>>> pE [Q](/PCIC/2023/test/pE.pdf) , [cpp](/PCIC/2023/r1/pE.cpp)  
>>>> 
>>> [r2](/PCIC/2023/r2/)
>>>> [Q](/PCIC/2023/r2/R2_All.pdf)  
>>>> [pA](/PCIC/2023/r2/pA.cpp)  
>>>> [pB](/PCIC/2023/r2/pB.cpp)  
>>>> [pC](/PCIC/2023/r2/pC.cpp)  
>>>> pD/[100](/PCIC/2023/r2/pD.cpp) ~~, pD/[013](/PCIC/2023/r2/pD-013.cpp) , pD/[031](/PCIC/2023/r2/pD-031.cpp)~~  
>>>> pE/[023](/PCIC/2023/r2/pE_bowle.cpp) , pE/[029](/PCIC/2023/r2/pE-old.cpp)
>>>> 
>>> [r3](/PCIC/2023/r3)  
>>>> [Q](/PCIC/2023/r2/R3_All.pdf)  
>>>> [pA](/PCIC/2023/r3/pA.cpp)  
>>>> [pB](/PCIC/2023/r3/pB.cpp)  
>>>> [pC](/PCIC/2023/r3/pC.cpp)  
>>>> pD/[070](/PCIC/2023/r3/pD.cpp)  
>>>> [pE](/PCIC/2023/r3/pE.cpp)  


未整理  
tioj_1419
